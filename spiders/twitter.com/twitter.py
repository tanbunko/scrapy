# coding: utf-8
"""
推特爬虫
"""
import traceback
import logging
import csv
import time
import json
import os
import sys
import pdb
import glob

BASE_DIR =  os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.append(BASE_DIR)

from lib.rule_spider import RuleSpider
from lib.csvutil import csvutil


class TwitterSpider(RuleSpider):
    """
    推特 爬虫
    """

    _log = logging.getLogger(__name__)

    def get_title_list(self, in_csv, out_csv):
        """
        run
        """
        # 读取输入csv文件
        targets = csvutil.read(in_csv, True, 'excel-tab')
        #open for write
        out = file(out_csv, 'wb')
        writer = csv.writer(out, 'excel-tab')

        out_title_list = ["name", "twitter_url", "title", "content_url"]

        writer.writerow(out_title_list)
        rules = self.read_rules("twitter_get_title_list_rule.json")

        for target in targets:
            try:
                rule = rules[0]
                # 初始化
                context = self.init_context(target['twitter_url'], rule)

                # 执行前期操作
                self.perform_section(rule["PrepareOps"], context)

                # 读取标题
                article_section = rule['article']
                elements = self.find_elements(article_section['Selector'])
                if elements:
                    for element in elements:
                        context['sect_data'] = {}
                        self.perform_section(article_section["GetSubSection"], context, element)
                        data = dict(target, **context['sect_data'])
                        writer.writerow(csvutil.dict2list(data, out_title_list))
                        out.flush()
            except Exception, e:
                self._log.error(traceback.format_exc())

            out.close()

    def get_content(self, in_csv, out_csv):
        """
        读取内容
        """
        # 读取输入csv文件
        targets = csvutil.read(in_csv, True, 'excel-tab')

        #open for write
        out = file(out_csv, 'wb')
        writer = csv.writer(out, 'excel-tab')

        out_title_list = ["name", "twitter_url", "title", "content_url", "content", "content_images", "cards"]
        writer.writerow(out_title_list)
        rules = self.read_rules("twitter_get_content_rule.json")

        for target in targets:
            try:
                rule = rules[0]
                # 初始化
                context = self.init_context(target['content_url'], rule)

                # 读取标题
                context['sect_data'] = {}
                self.perform_section(rule['content'], context)

                data = dict(target, **context['sect_data'])

                writer.writerow(csvutil.dict2list(data, out_title_list))            
                out.flush()
            except Exception, e:
                self._log.error(traceback.format_exc())
        
        out.close()

def main():
    # PROFILE_DIR = "/Users/yix/Library/Application Support/Firefox/Profiles/selenium"
    # SPIDER = TestSpider(browser="FireFox", profile_dir=PROFILE_DIR)
    SPIDER = TwitterSpider()

    # 读取标题及内容链接
    in_dir = "input"
    out_dir = "output/article_list"

    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    incsvs = glob.glob(in_dir + "/*.csv")
    for incsv in incsvs:
        filename = os.path.basename(incsv)
        output_file = out_dir + "/" + filename[:-4] + "_title.csv"

        # 读取输入csv文件
        targets = csvutil.read(incsv, True, 'excel-tab')

        twitter_list = dict()
        out = None
        writer = None
        for target in targets:
            twitter_id = target['twitter_url'].replace("https://twitter.com/", "").strip()

            if twitter_id and not twitter_list.has_key(twitter_id):
                if out:
                    out.close()
                output_file = out_dir + "/" + twitter_id + ".csv"
                twitter_list[twitter_id] = output_file
                SPIDER.get_title_list(incsv, output_file)


    # 读取内容
    in_dir = "output/article_list"
    out_dir = "output/result"

    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    incsvs = glob.glob(in_dir + "/*.csv")
    for incsv in incsvs:
        filename = os.path.basename(incsv)
        output_file = out_dir + "/" + filename[:-4] + "_result.csv"
        SPIDER.get_content(incsv, output_file)

def div_twitter():
    in_csv = "/Users/yix/prjs/scrapy/temp/twitter_url.title_list.csv"
    targets = csvutil.read(in_csv, True, 'excel-tab')
    out_title_list = ["name", "twitter_url", "title", "content_url"]

    twitter_list = dict()
    out = None
    writer = None
    for target in targets:
        twitter_id = target['twitter_url'].replace("https://twitter.com/", "").strip()
        if twitter_id:
            if not twitter_list.has_key(twitter_id):
                if out:
                    out.close()
                twitter_list[twitter_id] = '/Users/yix/prjs/scrapy/temp/' + twitter_id + ".csv"
                out = file(twitter_list[twitter_id], 'wb')
                writer = csv.writer(out, 'excel-tab')
                writer.writerow(out_title_list)
            else:
                writer.writerow(csvutil.dict2list(target, out_title_list))

            out.flush()

    out.close()

if __name__ == '__main__':
    main()