# coding: utf-8
"""
instagram爬虫
"""
import traceback
import logging
import csv
import time
import json
import os
import sys
import pdb

BASE_DIR =  os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.append(BASE_DIR)

from lib.rule_spider import RuleSpider
from lib.csvutil import csvutil


class InstagramSpider(RuleSpider):
    """
    Instagram爬虫
    """

    _log = logging.getLogger(__name__)

    def run(self, in_csv, out_dir):
        """
        run
        """
        # 读取输入csv文件
        targets = csvutil.read(in_csv, True, 'excel-tab')

        out_title_list = ["name", "ins_url", "datetime", "content", "images", "favs"]
        rules = self.read_rules("instagram_rule.json")

        if not os.path.exists(out_dir):
            os.makedirs(out_dir)

        for target in targets:
            if target['ins_url'] and target['name']:
                rule = rules[0]

                instagram_id = target['ins_url'].replace("https://www.instagram.com/", "").strip()

                output_file = out_dir + "/" + instagram_id + "_result.csv"

                #open for write
                out = file(output_file, 'wb')
                writer = csv.writer(out, 'excel-tab')
                writer.writerow(out_title_list)

                # 初始化
                context = self.init_context(target['ins_url'], rule)

                # 执行前期操作
                self.perform_section(rule["PrepareOps"], context)

                # 读取对话框内容
                has_more = True
                while has_more:
                    context['sect_data'] = {}
                    self.perform_section(rule['article'], context)

                    data = dict(target, **context['sect_data'])

                    writer.writerow(csvutil.dict2list(data, out_title_list))            

                    out.flush()

                    has_more = self.perform_section(rule['NextPage'], context)

                out.close()


if __name__ == '__main__':

    SPIDER = InstagramSpider()
    SPIDER.run("input/instagram_target.2.csv", "output")
