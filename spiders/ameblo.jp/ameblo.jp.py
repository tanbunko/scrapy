# coding: utf-8
"""
ameblo.jp
"""
import traceback
import logging
import csv
import time
import json
import os
import sys
import pdb

BASE_DIR =  os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.append(BASE_DIR)

from lib.rule_spider import RuleSpider
from lib.csvutil import csvutil


class AmebloSpider(RuleSpider):
    """
    Ameblo爬虫
    """

    _log = logging.getLogger(__name__)

    def run(self, in_csv, out_dir):
        """
        run
        """
        self._driver.maximize_window()
        # 读取输入csv文件
        targets = csvutil.read(in_csv, True, 'excel-tab')

        if not os.path.exists(out_dir):
            os.makedirs(out_dir)

        out_title_list = ["name", "ameba_url", "datetime", "content", "images"]

        rules = self.read_rules("ameblo_rule.json")

        for target in targets:
            if target['ameba_url'] and target['name']:
                rule = rules[0]

                ameba_id = target['ameba_url'].replace("http://ameblo.jp/", "").strip()

                output_file = out_dir + "/" + ameba_id + "_result.csv"

                #open for write
                out = file(output_file, 'wb')
                writer = csv.writer(out, 'excel-tab')
                writer.writerow(out_title_list)

                # 初始化
                context = self.init_context(target['ameba_url'], rule)

                # 执行前期操作
                try:
                    if rule.has_key("PrepareOps"):
                        self.perform_section(rule["PrepareOps"], context)
                except Exception,e:
                    self._log.error(traceback.format_exc())
                    continue

                # 读取对话框内容
                has_more = True
                while has_more:
                    context['sect_data'] = {}
                    self.perform_section(rule['article'], context)

                    data = dict(target, **context['sect_data'])

                    writer.writerow(csvutil.dict2list(data, out_title_list))

                    out.flush()
                    if rule.has_key("NextPage"):
                        has_more = self.perform_section(rule['NextPage'], context)
                    else:
                        has_more = False

                out.close()


if __name__ == '__main__':

    SPIDER = AmebloSpider("Chrome")
    SPIDER.run("input/ameblo.input.csv", "output")
