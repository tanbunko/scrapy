#encoding:utf8
import sys
sys.path.append("../..")

from lib.spider import Spider
from lib.csvutil import csvutil
import traceback
import logging
import csv

from selenium.webdriver.common.action_chains import ActionChains

import os
import string
import pdb

class ExciteSpider(Spider):
    """
    Translate Japanese information into Chinese information 
    """
    _log = logging.getLogger(__name__)

    _base_dir =  os.path.dirname(os.path.abspath(__file__))

    _site_id = "www.excite.co.jp"

    _translate_site = "http://www.excite.co.jp/world/chinese/"

    def run(self, target_csv, out_csv):
        #open target csv file
        targets = csvutil.read(target_csv, True)

        #open for write
        out = file(out_csv, 'wb')
        writer = csv.writer(out)
        title_list = ["商品编号","商品名","商品说明","源链接","商品名中文","商品说明中文"]
        translate_dict = {"商品名":"商品名中文", "商品说明":"商品说明中文"}
        
        writer.writerow(title_list)

        self._driver.get(self._translate_site)

        for target in targets:
            try:
                self._translate_all(target, title_list, translate_dict, writer)
            except :
                self._log.error(traceback.format_exc())
        
        out.close()

    
    def _clean_innerHTML(self, html):
        return html.replace("\t","").replace("\n","")

    def _translate(self, src):

        # 等待
        self.is_visable("div.search_dictionary")

        #取得 源翻译输入框
        el_leftBox = self.find_element("textarea#before")
        el_leftBox.clear()
        src_text = src
        if isinstance(src, str):
            src_text = unicode(src, "utf-8")
        elif isinstance(src, unicode):
            src_text = src

        el_leftBox.send_keys(src_text)

        el_exec_transfer_btn = self.find_element("input#exec_transfer")

        el_exec_transfer_btn.click()

        # 等待
        self.is_visable("div.search_dictionary")

        result = self.get_attribute("textarea#after", "innerHTML")

        self._log.debug(result)

        return result
        
    def _split_str(self, msg, max_len = 2048):
        """
        Split string to string list seperat by <br>
        """

        msgs = msg.replace("<br>","\t").split("\t")

        tmsgs = []
        i = 0

        tmp_msg = ""
        for ms in msgs:
            if len(tmp_msg) + len(ms) > max_len:
                tmsgs.append(tmp_msg)
                tmp_msg = ""
            else:
                tmp_msg = tmp_msg + ms + "\n"
        
        tmsgs.append(tmp_msg)

        return tmsgs


    def _translate_all(self, target, title_list, translate_dict, writer):
        item = target.copy()
        
        for key in translate_dict.keys():
            item_detail = ""
            src_list = self._split_str(item[key])
            for src in src_list:
                item_detail = item_detail + self._translate(src)

            item[translate_dict[key]] = item_detail.replace("\n","<br>")

        writer.writerow(csvutil.dict2list(item, title_list))


    def test(self):

        self._driver.get(_translate_site)

        self._translate("或いは")
        
        self._translate(u"私は田中です")



if __name__ == '__main__':
    a = ExciteSpider()
    a.run("input/rakuten_translation_file.csv", "output/rakuten_translation_file.out.csv")
    #a.run("input/rakuten_translation_file.1.csv", "output/rakuten_translation_file.1.out.csv")

