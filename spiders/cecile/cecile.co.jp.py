# coding: utf-8
"""
Cecile爬虫
"""
import traceback
import logging
import csv
import time
import json
import os
# import string
# import pdb
import sys
sys.path.append("../..")
from lib.spider import Spider
from lib.csvutil import csvutil
# from selenium.webdriver.common.action_chains import ActionChains

class CecileSpider(Spider):
    """
    Cecile spider class
    """
    _log = logging.getLogger(__name__)

    # 爬虫路径
    _base_dir = os.path.dirname(os.path.abspath(__file__))

    _site_id = "http://www.cecile.co.jp/"

    def run(self, target_csv, out_csv):
        """
        Run the Spider
        """
        #open target csv file
        targets = csvutil.read(target_csv, True)

        #open for write
        out = file(out_csv, 'wb')
        writer = csv.writer(out, 'excel-tab')
        title_list = ["goods_no", "title", "URL", "spec",\
             "detail", "size_table"]

        writer.writerow(title_list)

        for target in targets:
            try:
                self.get_item_detail(target, title_list, writer)
            except:
                print traceback.format_exc()
                self._log.error(traceback.format_exc())
        out.close()

    def clean_inner_html(self, html):
        """
        replace all \t and \n into "" in innerHTML
        """
        return html.replace("\t", "").replace("\n", "")


    def get_item_detail(self, target, title_list, writer):
        """
        Get item detail
        """
        item = target.copy()
        url = "http://www.cecile.co.jp/detail/" + target['goods_no']
        self._log.debug(url)

        self.get(url)

        item['URL'] = url

        # 等待，直到元素出现
        self.presence_of_element_located("div.itemdetail-box")

        #取得 标题
        item['title'] = self.get_innerText("h1.title")

        detail_es = self.get_all_elements_until_presence("div.iteminfo-box p")
        detail = self.get_innerText("p.description")
        for detail_e in detail_es:
            detail = detail + self.get_element_innerHTML(detail_e)
        item['detail'] = detail

        # 保存商品图片
        images = self.find_elements("div#imgbox div ul li img")

        order = 0
        for image in images:
            image_url = self.get_element_attribute(image, "src")
            self._log.info(image_url)
            dirname = "output/" + item['goods_no'].strip()
            filename = str(order) + "_" + image_url.replace("/", "_")\
                .replace(":", "_").replace("%", "_")
            self.download_image(image_url, os.path.join(dirname, filename))
            order += 1


        # 取得 价格
        color_select = self.get_select(\
            "div.cart div div dl.selection-box dd select[name=COLOR_SIZE_CD1]")
        size_select = self.get_select(\
            "div.cart div div dl.selection-box dd select[name=COLOR_SIZE_CD2]")

        spec = [] #规格
        # 有颜色，有尺寸
        if color_select is not None and size_select is not None:
            for color_option_element in color_select.options:
                color_value = self.get_element_attribute(color_option_element, "value")
                color_text = self.get_element_attribute(color_option_element, "innerText")
                if color_value != "":
                    color_option_element.click()
                    time.sleep(3)
                    for size_option_element in size_select.options:
                        size_value = self.get_element_attribute(size_option_element, "value")
                        size_text = self.get_element_attribute(size_option_element, "innerText")
                        if size_value != "":
                            size_option_element.click()
                            time.sleep(3)
                            product = {}
                            product['color'] = color_text
                            product['size'] = size_text
                            product['jp_price'] = self.get_innerText(\
                                "div.cart-box-in p#price span")
                            spec.append(product)
        # 有颜色，无尺寸
        elif color_select is not None and size_select is None:
            for color_option_element in color_select.options:
                color_value = self.get_element_attribute(color_option_element, "value")
                color_text = self.get_element_attribute(color_option_element, "innerText")
                if color_value != "":
                    color_option_element.click()
                    time.sleep(3)
                    product = {}
                    product['color'] = color_text
                    product['jp_price'] = self.get_innerText("div.cart-box-in p#price span")
                    spec.append(product)
        # 无颜色,有尺寸
        elif color_select is None and size_select is not None:
            for size_option_element in size_select.options:
                size_value = self.get_element_attribute(size_option_element, "value")
                size_text = self.get_element_attribute(size_option_element, "innerText")
                if size_value != "":
                    size_option_element.click()
                    time.sleep(3)
                    product = {}
                    product['size'] = size_text
                    product['jp_price'] = self.get_innerText(\
                        "div.cart-box-in p#price span")
                    spec.append(product)

        # 设定规格
        item['spec'] = json.dumps(spec)

        # 滚动到尺寸表
        iframe = self.presence_of_element_located("iframe.autoHeight")
        if iframe:
            self.scroll_to_element(iframe)

            # 获取文字尺寸表
            self._driver.switch_to.frame(0)
            size_table = self.find_element("table#size-table")
            item['size_table'] = self.clean_inner_html(\
                self.get_element_attribute(size_table, "innerHTML"))

            size_table_size = size_table.size


            # 将尺寸表保存为图片
            self._driver.switch_to.default_content()
            iframe = self.find_element("iframe.autoHeight")
            # self.set_element_attr(iframe, "height", size_table_size['height'] + 100)
            iframe_x = iframe.location['x']
            iframe_y = 0
            iframe_width = size_table_size['width'] + 1
            iframe_height = iframe.size['height'] + 1

            time.sleep(2)
            self.capture_rect(iframe_x, iframe_y, iframe_width, iframe_height, "output/" \
                + target['goods_no'] + "/" + "size_table.jpg")
        else:
            item['size_table'] = ""

        # 写入文件
        writer.writerow(csvutil.dict2list(item, title_list))



if __name__ == '__main__':
    THE_SPIDER = CecileSpider("Chrome")

    THE_SPIDER.run("input/input_goods_no.csv", "output/input_goods_no.out.csv")
