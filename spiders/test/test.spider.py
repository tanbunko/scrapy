# coding: utf-8
"""
测试爬虫
"""
import traceback
import logging
import csv
import time
import json
import os
import sys
import pdb


BASE_DIR =  os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.append(BASE_DIR)

from lib.spider import Spider
from lib.csvutil import csvutil


class TestSpider(Spider):

    _log = logging.getLogger(__name__)

    def test1(self):
        #open for write
        out = file("out.csv", 'wb')
        writer = csv.writer(out, 'excel-tab')

        page_url = "http://zasshi.news.yahoo.co.jp/article?a=20170306-00010000-otocoto-musi"
        rules = self.read_rules("site_rule.json")
        result = self.parse_page(page_url, rules)

        out_title_list = result.keys()
        writer.writerow(out_title_list)

        writer.writerow(csvutil.dict2list(result, out_title_list))

        out.close()


    def test2(self):
        #open for write
        out = file("twitter_out.csv", 'wb')
        writer = csv.writer(out, 'excel-tab')

        page_url = "https://twitter.com/HUAWEI_Japan_PR"

        self.get(page_url)

        username_box = self.find_element("#signin-dropdown > div.signin-dialog-body > form > div.LoginForm-input.LoginForm-username > input")
        password_box = self.find_element("#signin-dropdown > div.signin-dialog-body > form > div.LoginForm-input.LoginForm-password > input")
        login_btn = self.find_element("#signin-dropdown > div.signin-dialog-body > form > input.submit.btn.primary-btn.js-submit")

        usernameStr = 'tanbunko@gmail.com'
        passwordStr = 'tanwen'

        username_box.send_keys(usernameStr)
        password_box.send_keys(passwordStr)
        login_btn.click()

        self.scroll_until_done()

        # rules = self.read_rules("twitter_rule.json")

        # result = self.parse_page(page_url, rules)

        # out_title_list = result.keys()
        # writer.writerow(out_title_list)
        # writer.writerow(csvutil.dict2list(result, out_title_list))

        out.close()

    def test3(self):
        #open for write
        out = file("twitter_out.csv", 'wb')
        writer = csv.writer(out, 'excel-tab')

        page_url = "https://twitter.com/junebong"

        rules = self.read_rules("twitter_rule.json")
        title_list = "datetime, content, img"
        writer.writerow(title_list)
        result = self.parse_page(page_url, rules, title_list, writer)
        writer.writerow(csvutil.dict2list(result, title_list))

        out.close()

    def test_context_click(self):
        #open for write
        out = file("twitter_out.csv", 'wb')
        writer = csv.writer(out, 'excel-tab')

        page_url = "https://twitter.com/junebong"

        rules = self.read_rules("test_context_click.json")

        result = self.parse_page(page_url, rules)

        out_title_list = result.keys()
        writer.writerow(out_title_list)
        writer.writerow(csvutil.dict2list(result, out_title_list))

        out.close()
if __name__ == '__main__':

    # PROFILE_DIR = "/Users/yix/Library/Application Support/Firefox/Profiles/selenium"
    # SPIDER = TestSpider(browser="FireFox", profile_dir=PROFILE_DIR)
    SPIDER = TestSpider()
    SPIDER.test3()
