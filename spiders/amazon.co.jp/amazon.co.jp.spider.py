#encoding:utf8
import sys
sys.path.append("../..")

from lib.spider import Spider
from lib.csvutil import csvutil
import traceback
import logging
import csv

from selenium.webdriver.common.action_chains import ActionChains

import os
import string
import pdb

class AmazonJpSpider(Spider):

    _log = logging.getLogger(__name__)

    _base_dir =  os.path.dirname(os.path.abspath(__file__))

    _site_id = "www.amazon.co.jp"

    def run(self, target_csv, out_csv):
        #open target csv file
        targets = csvutil.read(target_csv, True)

        #open for write
        out = file(out_csv, 'wb')
        writer = csv.writer(out)
        title_list = ["JAN","title","URL", "price", "feature", "desc", "あらたｺｰﾄﾞ","商品名","ﾒｰｶｰｺｰﾄﾞ","ﾒｰｶｰ名","ケース入数","定価","納価（税抜き）","ｹｰｽ（幅）","ｹｰｽ（奥行）","ｹｰｽ（高さ）","ｹｰｽ（容量）","ｹｰｽ（重量）","大分類名","中分類名","小分類名"]
        
        writer.writerow(title_list)

        for target in targets:
            try:
                self.get_item_detail(target, title_list, writer)
            except :
                self._log.error(traceback.format_exc())
        
        out.close()

    def run2(self, target_csv, out_csv):
        """
        Read jan code from target csv file, then search and scrap the item information from amazon.co.jp.
        """
        #open target csv 
        targets = csvutil.read(target_csv, True)

        out = file(out_csv, 'wb')
        writer = csv.writer(out)
        title_list = ["JAN","title","URL", "price", "feature", "buckets", "desc"]

        writer.writerow(title_list)

        for target in targets:
            try:
                url = self.search_jan(target['JAN'])
                target['URL'] = url
                self.get_item_detail(target, title_list, writer)
            except:
                self._log.error(traceback.format_exc())
        
        out.close()

    
    def search_jan(self, jan):
        """ 
        Search jan code from https://www.amazon.co.jp/
        then return the first result link
        """
        jan = jan.strip()
        amazon_search_url = "https://www.amazon.co.jp/s/field-keywords=" + jan
        self._driver.get(amazon_search_url)

        # 等待，直到页脚元素出现
        self.is_visable("div#navFooter")

        self.capture_element("ul#s-results-list-atf li div.s-item-container", "output/" + jan + "/z_price_screenshot.jpg")

        url = self.get_attribute("ul#s-results-list-atf li div.s-item-container div div a.s-access-detail-page", "href")
        return url



    def _clean_innerHTML(self, html):
        return html.replace("\t","").replace("\n","")


    def get_item_detail(self, target, title_list, writer):
        item = target.copy()
        url = target['URL']
        self._log.debug(url)
        if not url:
            return 

        # 如果目标链接不是amazon.co.jp，则不作处理
        if not (self._site_id in url):
            self._log.info(url)
            return
        
        self._driver.get(url)

        # 等待，直到页脚元素出现
        self.is_visable("div#navFooter")

        #取得 标题
        item['title'] = self.get_innerText("span#productTitle")

        #取得 价格
        item['price'] = self.get_innerText("span#priceblock_ourprice")

        #取得 特性列表
        features = self.find_elements("div#featurebullets_feature_div div#feature-bullets ul li span.a-list-item")
        feature_str = '""'
        for feature in features:
            feature_str = feature_str +  self.get_element_innerHTML(feature) + ","
        
        feature_str = feature_str + '""'
        item['feature'] = feature_str

        #取得 商品描述
        item['desc'] = self._clean_innerHTML(self.get_innerHTML("div#productDescription p"))

        #取得 登録情報
        buckets = self.find_elements("div#detail_bullets_id table tbody tr td.bucket div.content ul li")
        bucket_tags = ["発送重量:","製造元リファレンス ：","ASIN:"]
        buckets_str = '""'
        for bucket in buckets:
            innerText = self.get_element_innerText(bucket)
            for bucket_tag in bucket_tags:
                if innerText.find(bucket_tag) != -1:
                    buckets_str = buckets_str +  self.get_element_innerText(bucket) + ","

        buckets_str = buckets_str + '""'
        item['buckets'] = buckets_str


        # 小图片悬停
        small_images = self.find_elements("div#altImages ul li.a-spacing-small.item.a-declarative")

        for small_image in small_images:
            self.hover_element(small_image)

        # 取大图并保存
        images = self.find_elements("li.image.item span span div.imgTagWrapper img")
        order = 0
        for image in images:
            image_url = self.get_element_attribute(image, "src")
            self._log.info(image_url)
            dirname = "output/" + item['JAN'].strip()
            filename = str(order) + "_" + image_url.replace("/","_").replace(":","_").replace("%","_")
            self.download_image(image_url, os.path.join(dirname, filename))
            order += 1


        writer.writerow(csvutil.dict2list(item, title_list))



if __name__ == '__main__':
    a = AmazonJpSpider()
    #a.run("input/arata_utf8.1.csv", "output/arata_utf8.1.csv")

    # a.run("input/arata_utf8.csv", "output/arata_utf8.csv")
    a.run2("input/input_jan_14000.csv", "output/input_jan_14000.out.csv")
