# coding=utf-8

import os
import string
import pdb
import traceback
import logging
import csv
import sys
sys.path.append("../..")

from lib.spider import Spider
from lib.csvutil import csvutil


from selenium.webdriver.common.action_chains import ActionChains


class AmazonJpSpider(Spider):
    '''
    Run
    '''

    _log = logging.getLogger(__name__)

    _base_dir = os.path.dirname(os.path.abspath(__file__))

    _site_id = "www.amazon.co.jp"

    def run_get_market_price(self, target_csv, out_csv):
        '''
        Run
        '''
        #open target csv file
        targets = csvutil.read(target_csv, True, 'excel-tab')

        #open for write
        out = file(out_csv, 'wb')
        writer = csv.writer(out)
        title_list = ["JAN",  "URL", "market_price"]

        writer.writerow(title_list)

        for target in targets:
            try:
                self.get_item_detail_market_price(target, title_list, writer)
            except:
                self._log.error(traceback.format_exc())

        out.close()


    def _clean_innerHTML(self, html):
        return html.replace("\t","").replace("\n","")



    def get_item_detail_market_price(self, target, title_list, writer):
        item = target.copy()
        url = target['URL']
        self._log.debug(url)
        if not url:
            return 

        #如果目标链接不是amazon.co.jp，则不作处理
        if not (self._site_id in url):
            self._log.info(url)
            return

        self._driver.get(url)

        #等待，直到页脚元素出现
        self.is_visable("div#navFooter")

        #取得 参考价格
        item['market_price'] = self.get_innerText("#price table tbody tr td.a-span12 span.a-text-strike")

        writer.writerow(csvutil.dict2list(item, title_list))

if __name__ == '__main__':
    a = AmazonJpSpider()

    a.run_get_market_price("input/test.csv", "output/test.out.csv")
