# coding: utf-8

import time
import pdb
import csv


from selenium import webdriver


class CecileTmallSpider:

    driver = None

    def __init__(self):
        print 'CecileTmallSpider'

    def __del__(self):
        CecileTmallSpider.close_driver()

    @staticmethod
    def driver():
        if CecileTmallSpider.driver is None:
            CecileTmallSpider.driver = webdriver.Firefox()
            CecileTmallSpider.driver.implicitly_wait(30)

        return CecileTmallSpider.driver

    @staticmethod
    def close_driver():
        if not CecileTmallSpider.driver:
            CecileTmallSpider.driver.quit()

    def getCategory(self, url, fileName):
        CecileTmallSpider.driver().get(url)
        
        category_links = CecileTmallSpider.driver().find_elements_by_css_selector(".cateAttrs .attr .attrValues ul li a")
        
        print len(category_links)
        
        
        csvfile = file(fileName, 'wb')
        
        writer = csv.writer(csvfile)

        writer.writerow(['类名', 'URL'])

        categories = []
    
        for clink in category_links:
            category = {'name':clink.get_attribute('innerHTML'), 'url' : clink.get_attribute('href')}
            categories.append(category)
            writer.writerow([category['name'], category['url']])
        
        csvfile.close()

        return categories

    def get_all_items(self, url, writer):
        CecileTmallSpider.driver().get(url)

        self.get_items(writer)

    def get_items(self, writer):
        
        item_links = CecileTmallSpider.driver().find_elements_by_css_selector('div#J_ShopSearchResult div.skin-box-bd div.J_TItems div.item5line1 dl.item')
        
        for item_link in item_links:
            try:
                item = {'id': item_link.get_attribute('data-id'),  'url': item_link.find_element_by_css_selector('dt.photo a').get_attribute('href') }
            except Exception, e:
                pdb.set_trace()
                print e
            
            writer.writerow([item['id'], item['url']])
        
        try:
            next_link = self.driver.find_element_by_css_selector('div.pagination a.next')
        except Exception:
            # end
            return
            
        next_link.click()
        
        self.get_items(writer)

    def get_items_detail(self, csv_file_name):
        
        csvfile = file(csv_file_name, 'rb')
        reader = csv.reader(csvfile)

        for line in reader:
            self.get_item_detail(line[0], line[1])

    def get_item_detail(self, item_id, item_url):
        print item_id + item_url
        


spider = CecileTmallSpider()

#first_page_url = 'https://cecile.tmall.hk/search.htm?spm=a1z10.1-b-s.w11794954-14762574754.1.GUtxkA&scene=taobao_shop'

#categories = spider.getCategory(first_page_url, 'categories.csv')

#csvfile = file("items.csv", 'wb')

#writer = csv.writer(csvfile)

#writer.writerow(['ID', 'URL'])

#spider.get_all_items(first_page_url, writer)

#csvfile.close()

spider.get_items_detail("items.csv")


