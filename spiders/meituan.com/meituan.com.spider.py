#encoding:utf8
import os
import string
import pdb
import json
import re
import logging
import csv
import sys
THE_BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
sys.path.append(THE_BASE_DIR)

from lib.spider import Spider
from lib.csvutil import csvutil

_DONE_FLAG = {}
_MAX_COUNT = 300

class MeituanSpider(Spider):

    _log = logging.getLogger(__name__)


    def get_all_shop_list(self, shop_url, output):
        '''
        Get all shop list.
        '''

        out = file(output, 'wb')
        writer = csv.writer(out, 'excel-tab')

        csv_title_list = ["name", "url"]

        writer.writerow(csv_title_list)

        self._get_shop_list(shop_url, writer, csv_title_list)

        out.close()


    def _get_shop_list(self, shop_url, writer, csv_title_list):
        """
        Get shop list of meituan.
        """
        self._driver.get(shop_url)

        self.presence_of_element_located("div#content")

        self.scroll_until_done()


        csv_shop = {}

        # 有deal商家
        shop_lists = self.find_elements("div.deal-tile")

        for shop in shop_lists:
            tag = self.find_sub_element(shop, "a.deal-tile__cover")
            csv_shop['url'] = self.get_element_attribute(tag, "href")
            tag = self.find_sub_element(shop, "h3.deal-tile__title span.xtitle")
            csv_shop['name'] = self.get_element_attribute(tag, "innerText")

            writer.writerow(csvutil.dict2list(csv_shop, csv_title_list))


        # 无deal商家
        shop_lists = self.find_elements("div.poi-tile-nodeal")

        for shop in shop_lists:
            tag = self.find_sub_element(shop, "a.poi-tile__head")
            csv_shop['url'] = self.get_element_attribute(tag, "href")
            tag = self.find_sub_element(shop, "div.poi-tile__info div a.link")
            csv_shop['name'] = self.get_element_attribute(tag, "innerText")

            writer.writerow(csvutil.dict2list(csv_shop, csv_title_list))

        next_url = self.get_attribute("ul.paginator li.next a", "href")

        if next_url != '':
            self._get_shop_list(next_url, writer, csv_title_list)

    def _do_shop_list(self, url, csv_title_list, writer):
        if url.find("/deal/") > -1:
            self._driver.get(url)
            deal_component = self.presence_of_element_located("div.deal-component-container")

            if deal_component:
                business_info = self.find_element("div.component-business-info")
                if business_info:
                    self.scroll_to_element(business_info)
                    self.presence_of_element_located("h5.biz-info__title a.poi-link")

                    the_shop_url = self.get_sub_element_attribute(
                        business_info,
                        "h5.biz-info__title a.poi-link",
                        "href")

                    if the_shop_url:
                        self._get_shop_info(the_shop_url, csv_title_list, writer)

        elif url.find("/shop/") > -1:
            self._get_shop_info(url, csv_title_list, writer)


    def _get_shop_info(self, shop_url, title_list, writer):
        """
        Get shop info
        """
        pshopurl = re.compile('.*?\\/shop\\/(\\d+).*')

        shopmatch = pshopurl.match(shop_url)


        if shopmatch is None:
            return

        shopid = shopmatch.group(1)

        if _DONE_FLAG.has_key(shopid):
            return

        self._driver.get(shop_url)

        shop_summary = self.presence_of_element_located("div.summary")

        self.scroll_until_done()

        if shop_summary:
            # shopinfo['name'] = self.get_sub_element_attribute(
            #     shop_summary,
            #     "h2 span.title",
            #     "innerText")
            # shopinfo['address'] = self.get_sub_element_attribute(
            #     shop_summary,
            #     "p.under-title span.geo",
            #     "innerText")
            shop_detail = self.find_element("div#content div.poi-section--shop")

            if shop_detail:
                img_url = self.get_sub_element_attribute(shop_detail, "a.biz-picture span img", "src")
                description = self.get_sub_element_attribute(shop_detail, "div#poi-description span.long-biz-info", "innerText")


            shops_json = self.get_sub_element_attribute(
                shop_summary,
                "span#map-canvas",
                "data-params")
            if shops_json:
                shops_info = json.loads(shops_json)

                shops = shops_info["shops"]
                for shopid in shops:
                    shop = shops[shopid]
                    shop['id'] = shopid
                    shop['img_url'] = img_url
                    shop['description'] = description
                    _DONE_FLAG[shopid] = True

                    writer.writerow(csvutil.dict2list(shop, title_list))



def do_shop_list(input_csv, output_csv):
    """
    Do shop list
    """
    out = file(output_csv, 'wb')
    writer = csv.writer(out, 'excel-tab')

    csv_title_list = ["id", "name", "img_url", "address", "description", "phone", "position"]

    writer.writerow(csv_title_list)

    shop_url_list = csvutil.read(input_csv, True, 'excel-tab')

    count = 0

    for shop_url in shop_url_list:
        if count % _MAX_COUNT == 0:
            MEITUAN = MeituanSpider("Chrome")

        MEITUAN._do_shop_list(shop_url['url'], csv_title_list, writer)
        count += 1

    out.close()


if __name__ == '__main__':
    # print THE_BASE_DIR
    # MEITUAN.get_all_shop_list(
    #     "http://hz.meituan.com/category/?mtt=1.index%2Fdefault%2Ffilter.0.0.iw4poqha",
    #     "output/shop_list.o.csv")

    do_shop_list("input/shop_list.csv", "output/shop_info.o.csv")


