#encoding:utf8
import sys
sys.path.append("../..")

from lib.spider import Spider
from lib.csvutil import csvutil
import traceback
import logging
import csv
import re

import os
import string
import pdb

class CosmeSpider(Spider):

    _log = logging.getLogger(__name__)

    _base_dir =  os.path.dirname(os.path.abspath(__file__))

    _site_id = "http://www.cosme.net/"

    _pages = [ '', '/page/1', '/page/2','/page/3', '/page/4']

    def run(self, input_csv, output_csv):

        #open for write
        out = file(output_csv, 'wb')
        writer = csv.writer(out)
        title_list = ["分类", "更新日期", "排名","商品ID", "商品标题", "商品简介", "URL", "weight", "price"]
        
        writer.writerow(title_list)

        targets = csvutil.read(input_csv, True, 'excel-tab')

        for target in targets:
            for page in self._pages:
                try:
                    self._get_item_info(target['Category'], target['URL'] + page, title_list, writer)
                except Exception as e:
                    self._log.error(e.message)
        
        out.close()

    def _get_item_info(self, category, target_url, title_list,  writer):
        self._driver.get(target_url)
        self._log.info(target_url)

        self.is_visable("div.keyword-ranking-icon-info")

        items = []

        
        p = re.compile('http://www.cosme.net/product/product_id/(\d+)\/')
        p_item_price = re.compile(u'(.*?)g\u30fb(.*?)\u5186')

        rank_item_list = self.find_elements("div#keyword-ranking-list div.keyword-ranking-item")
        for rank_item in rank_item_list:
            item = {}
            item['分类'] = category
            item['更新日期'] = self.get_innerText("div#keyword-ranking-header p span")
            rank_num = self.find_sub_element(rank_item, "span.rank_num span.num")
            item['排名'] = self.get_element_innerText(rank_num)
            pdb.set_trace()
            item_title_link = self.find_sub_element(rank_item, "dd.summary span.item a")
            item['商品标题'] = self.get_element_innerText(item_title_link)
            item['URL'] = self.get_element_attribute(item_title_link, "href")
            m =  p.match(item['URL'])
            item['商品ID'] = m.group(1)
            items.append(item)


        for item in items:

            self._driver.get(item['URL'])
            self._log.info(item['URL'])
            self.is_visable("div#product-spec")

            item['商品简介'] = self.get_attribute("div#product-spec dl#mdDesc dd", "innerText")
            img_page_url = self.get_attribute("div#product-info div#mdImg a", "href")
            
            item_infos = self.find_elements("div#product-info div.info div ul li p")
            item['weight'] = 0
            item['price'] = 0
            
            for item_info in item_infos:
                info = self.get_element_attribute(item_info, "innerText")
                m = p_item_price.match(info)
                if m:
                    item['weight'] = m.group(1)
                    item['price'] = m.group(2)
                    break
            
            self._driver.get(img_page_url)
            self.is_visable("div#product-view-pht-list")
            
            photos = self.find_elements("div#product-view-detail ul li img")
            dirname = "output/" + item['商品ID'].strip()
            
            for photo in photos:
	            photo_url = self.get_element_attribute(photo, "src")
	            filename = photo_url.replace("/","_").replace(":","_").replace("%","_")
	            self.download_image(photo_url, os.path.join(dirname, filename))

            writer.writerow(csvutil.dict2list(item, title_list))



if __name__ =='__main__':  
    print 'cosme spider'
    spider = CosmeSpider()
    spider.run("input/target.csv", "output/target.out.csv")


