# coding: utf-8
"""
Yahoo.co.jp爬虫
爬取艺人新闻
"""
import traceback
import logging
import csv
import time
import json
import os
# import string
import sys
sys.path.append("../..")
from lib.spider import Spider
from lib.csvutil import csvutil
# from selenium.webdriver.common.action_chains import ActionChains

import pdb

class YahooSpider(Spider):
    """
    Yahoo spider class
    """
    _log = logging.getLogger(__name__)

    # 爬虫路径
    _base_dir = os.path.dirname(os.path.abspath(__file__))

    _follow_themes = "https://follow.yahoo.co.jp/themes/"
    _follow_themes_api = \
        "https://follow.yahoo.co.jp/api/themes/{#uid#}/articles?page={#page#}&pageuri=%2Fdetail%2Farticle"
    _news_search = "https://news.yahoo.co.jp/search/"

    def run(self, in_csv, out_csv):
        """
        读取文章列表
        """
        # 读取输入csv文件
        targets = csvutil.read(in_csv, True, 'excel-tab')

        #open for write
        out = file(out_csv, 'wb')
        writer = csv.writer(out, 'excel-tab')
        out_title_list = ["company", "name", "url", "topic_title", "source", "topic_url"]

        writer.writerow(out_title_list)

        for target in targets:
            try:
                self.get_topic_list(target, out_title_list, writer)
            except:
                print traceback.format_exc()
                self._log.error(traceback.format_exc())
        out.close()

    def clean_inner_html(self, html):
        """
        replace all \t and \n into "" in innerHTML
        """
        return html.replace("\t", "").replace("\n", "")


    def get_topic_list(self, target, title_list, writer):
        """
        Get topic list
        """
        topic = target.copy()

        url = target['url']

        if self._follow_themes in url:
            self.get_topic_list_follow_themes(topic, title_list, writer)
        elif self._news_search in url:
            self.get_topic_list_news_search(topic, title_list, writer)
        else:
            self._log.error("无法解析该链接:" + url)
            return

    def get_topic_list_follow_themes(self, topic, title_list, writer):
        """
        get topic list from follow themes link
        """
        url = topic['url']
        uid = url.replace(self._follow_themes, "")
        api_url = self._follow_themes_api.replace("{#uid#}", uid)

        for page in range(1, 100):
            page_url = api_url.replace("{#page#}", str(page))
            self.get(page_url)

            if not self.presence_of_element_located("body>li>a>div.ta>p.entry"):
                if self.find_element("div.contents>div.notfound"):
                    return
                else:
                    self._log.error("找不到爬虫目标信息：" + api_url)
                    continue

            feeds = self.find_elements("body>li>a")

            for feed in feeds:
                topic['topic_title'] = self.get_sub_element_attribute(feed, "p.entry", "innerText")
                topic['source'] = self.get_sub_element_attribute(feed, "p.source", "innerText")
                topic['topic_url'] = self.get_element_attribute(feed, "href")

                # 写入文件
                writer.writerow(csvutil.dict2list(topic, title_list))


    def get_topic_list_news_search(self, topic, title_list, writer):
        """
        get topic list from yahoo news link
        """
        url = topic['url']

        for page in range(1, 1000, 10):
            page_url = url + "&b=" + str(page)

            self.get(page_url)

            if not self.presence_of_element_located(\
                "div#contents>div.uWrap>div>div#mIn>div#NSm>div"):
                if self.find_element("div#mIn>div#S1ak>p>em"):
                    return
                else:
                    self._log.error("找不到爬虫目标信息：" + page_url)
                    continue

            feeds = self.find_elements("div#contents>div.uWrap>div>div#mIn>div#NSm>div")

            for feed in feeds:
                topic['topic_title'] = self.get_sub_element_attribute(feed, "h2.t>a", "innerText")
                topic['source'] = self.get_sub_element_attribute(feed, \
                    "div.txt>p>span.d", "innerText")
                topic['topic_url'] = self.get_sub_element_attribute(feed, "h2.t>a", "href")

                # 写入文件
                writer.writerow(csvutil.dict2list(topic, title_list))


    def get_content(self, in_csv, out_csv):
        """
        读取文章列表
        """
        # 读取输入csv文件
        targets = csvutil.read(in_csv, True, 'excel-tab')

        #open for write
        out = file(out_csv, 'wb')
        writer = csv.writer(out, 'excel-tab')
        out_title_list = ["company", "name", "url", "topic_title", "source", "topic_url",\
             "title", "content", "image"]

        writer.writerow(out_title_list)

        # 读取爬虫规则
        rules = self.read_rules("site_rules.json")

        # 解析网页
        for target in targets:
            try:
                page_url = target['topic_url']
                data = self.parse_page(page_url, rules)
                if not data:
                    continue

                output_data = dict(target, **data)
                writer.writerow(csvutil.dict2list(output_data, out_title_list))
            except:
                print traceback.format_exc()
                self._log.error(traceback.format_exc())


        out.close()


    def test(self):
        #open for write
        out = file("output/test.out.csv", 'wb')
        writer = csv.writer(out, 'excel-tab')
        out_title_list = ["title", "content", "image"]

        writer.writerow(out_title_list)

        rules = self.read_rules("site_rules.json")

        # page_url = "http://www.afpbb.com/articles/-/3118395"
        # data = self.parse_page(page_url, rules)
        # writer.writerow(csvutil.dict2list(data, out_title_list))

        page_url = "http://zasshi.news.yahoo.co.jp/article?a=20170307-00010000-jisin-ent"
        data = self.parse_page(page_url, rules)
        writer.writerow(csvutil.dict2list(data, out_title_list))

if __name__ == '__main__':
    THE_SPIDER = YahooSpider("Chrome")

    # THE_SPIDER.run("input/url_list.csv", "output/url_list.out.csv")
    THE_SPIDER.get_content("input/url_list.out.csv", "output/topics.csv")
    # THE_SPIDER.test()