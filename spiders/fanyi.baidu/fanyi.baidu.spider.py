#encoding:utf8
import sys
sys.path.append("../..")

import urllib2

from lib.spider import Spider
from lib.csvutil import csvutil
import traceback
import logging
import csv
from time import sleep

from selenium.webdriver.common.action_chains import ActionChains

import os
import string
import pdb

class FanyiBaiduSpider(Spider):
    """
    Translate Japanese information into Chinese information 
    """
    _log = logging.getLogger(__name__)

    _base_dir =  os.path.dirname(os.path.abspath(__file__))

    _site_id = "fanyi.baidu.com"

    _translate_site = "http://fanyi.baidu.com/#jp/zh/"

    def run(self, target_csv, out_csv):
        #open target csv file
        targets = csvutil.read(target_csv, True)

        #open for write
        out = file(out_csv, 'wb')
        writer = csv.writer(out)
        title_list = ["商品编号","商品名","商品说明","商品名中文","商品说明中文"]
        translate_dict = {"商品名":"商品名中文", "商品说明":"商品说明中文"}
        
        writer.writerow(title_list)

        self._driver.get(self._translate_site)

        for target in targets:
            try:
                self._translate_all(target, title_list, translate_dict, writer)
            except :
                self._log.error(traceback.format_exc())
        
        out.close()

    
    def _clean_innerHTML(self, html):
        return html.replace("\t","").replace("\n","")

        
    def _split_str(self, msg, max_len = 5000):
        """
        Split string to string list seperat by <br>
        """

        msgs = msg.replace("<br>","\t").split("\t")

        tmsgs = []
        i = 0

        tmp_msg = ""
        for ms in msgs:
            if len(tmp_msg) + len(ms) > max_len:
                tmsgs.append(tmp_msg)
                tmp_msg = ""
            else:
                tmp_msg = tmp_msg + ms + "\n"
        
        tmsgs.append(tmp_msg)

        return tmsgs


    def _translate_all(self, target, title_list, translate_dict, writer):
        item = target.copy()
        
        for key in translate_dict.keys():
            item_detail = ""
            src_list = self._split_str(item[key])
            for src in src_list:
                item_detail = item_detail + self._translate_jp_zn(src)

            item[translate_dict[key]] = item_detail.replace("\n","<br>")

        writer.writerow(csvutil.dict2list(item, title_list))

    def _translate_jp_zn(self, src):
        src_text = src
        if isinstance(src, str):
            src_text = src
        elif isinstance(src, unicode):
            src_text = src.encode('utf-8')

        url = self._translate_site + urllib2.quote(src_text)

        self._driver.get(url)

        # 等待
        self.is_visable("span.copyright-text")

        self.click_element_until_clickable("a#translate-button")

        sleep(5)


        return self.get_innerText("div.output-bd")



    def test(self):

        print self._translate_jp_zn("私は田中です")

        print self._translate_jp_zn(u"おはようございます\nあかるい")


if __name__ == '__main__':
    a = FanyiBaiduSpider()
    a.run("input/translate_goods.1.csv", "output/translate_goods.out.1.csv")


