# coding: utf-8

import pdb
import csv
import cPickle as pickle

class csv2json:

    @staticmethod
    def read(csv_filename, has_header = True) :
        """
            Open and read a csv file

            Args:
                csv_filename: Csv filename
                has_header: the first line is col header
            Return:
                if has header row then [{'col0_title': col0, 'col1_title': col1, ...}, {'col0_title': col0, 'col1_title': col1, ...}] will be return
                if hasnot header then [col0, col1, ...] will be return
        """

        if has_header :
            return csv2json.__read_has_header(csv_filename)
        else:
            return csv2json.__read_no_header(csv_filename)
                
    @staticmethod
    def __read_no_header(csv_filename):
        csvfile = file(csv_filename, 'rb')
        lines = csv.reader(csvfile)
        rows = []
        for line in lines:
            rows.append(line)
        
        csvfile.close()
        return rows

    @staticmethod
    def __read_has_header(csv_filename):
        print "__read_has_header"
        csvfile = file(csv_filename, 'rb')
        lines = csv.reader(csvfile)
        
        results = []
        col_titles = []
        current_line = 0
        for line in lines:
            if current_line == 0:
                for col_title in line:
                    col_titles.append(col_title)
            else:
                obj ={}
                for index in range(0, len(line)):
                    obj[col_titles[index]] = line[index]
                results.append(obj)

            current_line = current_line + 1

        csvfile.close()
        return results

if __name__ =='__main__':  
    obj1 = csv2json.read('cosme.csv', False)
    obj2 = csv2json.read('cosme.header.csv', True)
    pdb.set_trace()
    