# coding: utf-8

import sys
sys.path.append('../lib/')

import time
import pdb
import csv

from csv2json import csv2json
import StreamToLogger
import logging
import traceback

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
import selenium.webdriver.support.expected_conditions as EC
import selenium.webdriver.support.ui as ui



class MatsukiyoSpider:

    driver = None

    def __init__(self):
        print '__init__'
        self.driver = webdriver.Firefox()
        self.wait = ui.WebDriverWait(self.driver,10)

    def __del__(self):
        print '__del__'
        self.driver.quit()

    def get_item_detail(self, target_csv_filename, out_filename):
        print 'get_item_detail:' + target_csv_filename

        targets = csv2json.read(target_csv_filename, False)

        out_csv = file(out_filename, 'wb')
        writer = csv.writer(out_csv)

        for target in targets:
            try:
                name = target[0]
                url = target[1]
                self.driver.get(url)

                self.is_visible("section.item__main__detail div.item__main__detail__detail p.item__main__detail__text")

                target_item_details = self.driver.find_elements_by_css_selector("section.item__main__detail div.item__main__detail__detail p.item__main__detail__text")

                item_detail = [name, url]
        
                for target_item_detail in target_item_details:
                    item_property = target_item_detail.get_attribute('innerHTML').replace("\t","").replace("\r","").replace("\n","")
                    item_detail.append(item_property)

                print u"item:" + item_detail[0]

                writer.writerow(item_detail)
            
            except Exception, e:
                logging.error(traceback.format_exc())

                
        out_csv.close()


    def get_all_items(self, target_csv_filename, out_filename):
        print 'get_all_items'

        targets = csv2json.read(target_csv_filename, True)

        index = 0

        for target in targets:
            self.get_item_list(str(index) + out_filename, target["category"], target["url"])
            index = index + 1

        csvfile.close()

    def get_item_list(self, out_filename, name, url):

        print u'get item list for ' + name + ',' + url
        out_csv = file(out_filename, 'wb')
        writer = csv.writer(out_csv)

        self.driver.get(url)

        current_page = 1

        self.get_item_list_of_page(writer, current_page)

        out_csv.close()

    # return True if element is visible within 5 seconds, otherwise False
    def is_visible(self, locator, timeout=5):
        try:
            self.wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, locator)))
            return True
        except TimeoutException:
            return False

    # return True if element is not visible within 5 seconds, otherwise False
    def is_not_visible(self, locator, timeout=5):
        try:
            self.wait.until_not(EC.visibility_of_element_located((By.CSS_SELECTOR, locator)))
            return True
        except TimeoutException:
            return False

    def get_item_list_of_page(self, writer, current_page):

        # read item information
        print 'current page:%d' %(current_page)

        self.is_visible("ul.main__categoryLink li.main__categoryLink__item")

        #self.wait.until(lambda driver: driver.find_element_by_css_selector('ul.main__categoryLink li.main__categoryLink__item'))

        target_items = self.driver.find_elements_by_css_selector("li.main__itemList__item div.itemContainer div.itemContainer__detail h4.itemContainer__title a")

        for target_item in target_items:
            href = target_item.get_attribute("href")
            name = target_item.get_attribute("innerHTML").replace('\t','').replace('\n','')
            writer.writerow([name, href])

        # click li to next page
        next_page = current_page + 1
        next_page_nation = None

        try:

            page_nation_tag = self.driver.find_element_by_css_selector('nav.pageNation')
            page_nation_list = page_nation_tag.find_elements_by_css_selector('nav.pageNation ul.pageNation__list li a')

            for page_nation in page_nation_list:
                if int(page_nation.get_attribute('innerHTML')) == next_page :
                    next_page_nation = page_nation
                    break

            if next_page_nation is not None:
                next_page_nation.click()
                self.get_item_list_of_page(writer, next_page)

        except Exception, e:
            logging.error(traceback.format_exc())



spider = MatsukiyoSpider()

#取得商品列表
#spider.get_all_items("item_list.csv", "_item_list.csv")

#取得商品详细
#input_filename = "0_output.csv"
#spider.get_item_detail(input_filename, "item_detail_" + input_filename)

input_filename = "1_output.csv"
spider.get_item_detail(input_filename, "item_detail_" + input_filename)
input_filename = "2_output.csv"
spider.get_item_detail(input_filename, "item_detail_" + input_filename)
input_filename = "3_output.csv"
spider.get_item_detail(input_filename, "item_detail_" + input_filename)
input_filename = "4_output.csv"
spider.get_item_detail(input_filename, "item_detail_" + input_filename)
input_filename = "5_output.csv"
spider.get_item_detail(input_filename, "item_detail_" + input_filename)
input_filename = "6_output.csv"
spider.get_item_detail(input_filename, "item_detail_" + input_filename)
input_filename = "7_output.csv"
spider.get_item_detail(input_filename, "item_detail_" + input_filename)
input_filename = "8_output.csv"
spider.get_item_detail(input_filename, "item_detail_" + input_filename)
input_filename = "9_output.csv"
spider.get_item_detail(input_filename, "item_detail_" + input_filename)
input_filename = "10_output.csv"
spider.get_item_detail(input_filename, "item_detail_" + input_filename)
input_filename = "11_output.csv"
spider.get_item_detail(input_filename, "item_detail_" + input_filename)
input_filename = "12_output.csv"
spider.get_item_detail(input_filename, "item_detail_" + input_filename)
input_filename = "13_output.csv"
spider.get_item_detail(input_filename, "item_detail_" + input_filename)
input_filename = "14_output.csv"
spider.get_item_detail(input_filename, "item_detail_" + input_filename)
input_filename = "15_output.csv"
spider.get_item_detail(input_filename, "item_detail_" + input_filename)
input_filename = "16_output.csv"
spider.get_item_detail(input_filename, "item_detail_" + input_filename)
input_filename = "17_output.csv"
spider.get_item_detail(input_filename, "item_detail_" + input_filename)
input_filename = "18_output.csv"
spider.get_item_detail(input_filename, "item_detail_" + input_filename)
input_filename = "19_output.csv"
spider.get_item_detail(input_filename, "item_detail_" + input_filename)
input_filename = "20_output.csv"
spider.get_item_detail(input_filename, "item_detail_" + input_filename)
input_filename = "21_output.csv"
spider.get_item_detail(input_filename, "item_detail_" + input_filename)
input_filename = "22_output.csv"
spider.get_item_detail(input_filename, "item_detail_" + input_filename)
input_filename = "23_output.csv"
spider.get_item_detail(input_filename, "item_detail_" + input_filename)
input_filename = "24_output.csv"
spider.get_item_detail(input_filename, "item_detail_" + input_filename)
input_filename = "25_output.csv"
spider.get_item_detail(input_filename, "item_detail_" + input_filename)
input_filename = "26_output.csv"
spider.get_item_detail(input_filename, "item_detail_" + input_filename)
input_filename = "27_output.csv"
spider.get_item_detail(input_filename, "item_detail_" + input_filename)
input_filename = "28_output.csv"
spider.get_item_detail(input_filename, "item_detail_" + input_filename)
input_filename = "29_output.csv"
spider.get_item_detail(input_filename, "item_detail_" + input_filename)
input_filename = "30_output.csv"
spider.get_item_detail(input_filename, "item_detail_" + input_filename)
input_filename = "31_output.csv"
spider.get_item_detail(input_filename, "item_detail_" + input_filename)
