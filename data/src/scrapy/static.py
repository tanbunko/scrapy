class test:
    val = 0
    
    def test1(self):
        self.val = 1
        return 'error'
        
    def test2(self):
        return self.val
        
    def test3(self):
        return test.val
    
    @classmethod
    def test4(self):
        self.val = 4
        return self.val
        
    @staticmethod
    def test5():
        return test.val
        
    def __init__(self):
        print '__init__'

    def __del__(self):
        print "__del__"
        print test.test5()
    
t = test()

#print t.test1()
#print t.test2()
#print t.test3()
#print test.test4()
#test.val = 1
#print test.test5()

