# coding: utf-8
"""
类初始化
"""
import sys
import os
import datetime
import logging

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
LIB_DIR = BASE_DIR + "/lib"
sys.path.append(LIB_DIR)

# 重新加载sys模块
reload(sys)
# 设置默认编码模式
sys.setdefaultencoding('utf-8')

import log4p

# 通过设置文件的方式设置日志
CONF_LOG = BASE_DIR + "/conf/log.conf"
logging.config.fileConfig(CONF_LOG, defaults={'TODAY': str(datetime.date.today())[0:10]})

#log4p.init_logger()
