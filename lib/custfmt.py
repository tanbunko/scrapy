#encoding:utf8

import logging

class CustomFormatter(logging.Formatter):
    def __init__(self, default):
        self.default = default

    def format(self, record):        
        if isinstance(record.msg, str):
            # 将日志信息中的回车替换为{{LF}}
            record.msg = record.msg.replace("\n","{{LF}}")
        return self.default.format(record)

def factory(fmt, datefmt):
    default = logging.Formatter(fmt, datefmt)
    return CustomFormatter(default)