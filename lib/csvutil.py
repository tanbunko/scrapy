# coding: utf-8

import pdb
import csv
import cPickle as pickle

class csvutil:

    @staticmethod
    def read(csv_filename, has_header = True, dialect = 'excel'):
        """
            Open and read a csv file

            Args:
                csv_filename: Csv filename
                has_header: the first line is col header
                dialect: 'excel' or 'excel-tab'
            Return:
                if has header row then [{'col0_title': col0, 'col1_title': col1, ...}, {'col0_title': col0, 'col1_title': col1, ...}] will be return
                if hasnot header then [col0, col1, ...] will be return
        """

        if has_header :
            return csvutil.__read_has_header(csv_filename, dialect)
        else:
            return csvutil.__read_no_header(csv_filename, dialect)
                
    @staticmethod
    def __read_no_header(csv_filename, dialect = 'excel'):
        csvfile = file(csv_filename, 'rU')
        lines = csv.reader(csvfile, dialect)
        rows = []
        for line in lines:
            rows.append(line)
        
        csvfile.close()
        return rows

    @staticmethod
    def __read_has_header(csv_filename, dialect = 'excel'):
        csvfile = file(csv_filename, 'rU')
        lines = csv.reader(csvfile, dialect)
        
        results = []
        col_titles = []
        current_line = 0
        for line in lines:
            if current_line == 0:
                for col_title in line:
                    col_titles.append(col_title)
            else:
                obj ={}
                for index in range(0, len(line)):
                    obj[col_titles[index]] = line[index]
                results.append(obj)

            current_line = current_line + 1

        csvfile.close()
        return results

    @staticmethod
    def dict2list(dict_src, key_list):
        ls = []
        for key in key_list:
            ls.append(dict_src[key])
        return ls

if __name__ =='__main__':  
    obj1 = csvutil.read('cosme.csv', False)
    obj2 = csvutil.read('cosme.header.csv', True)
    pdb.set_trace()
    