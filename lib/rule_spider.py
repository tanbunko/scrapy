# coding: utf-8
#import sys
"""
规则爬虫基类
需求：
    selenium
    PIL
    requests
"""
import json
import re
import time
import os
import traceback
import util
import spider
import pdb


class RuleSpider(spider.Spider):
    """
    读取规则json文件,并按规则爬取内容
    """


    def read_rules(self, rule_json_file):
        """
        读取网站解析规则JSON文件
        """
        infile = open(rule_json_file, 'rb')
        rules = json.load(infile)
        infile.close()
        return rules


    def parse_page(self, page_url, rules):
        """
        使用规则解析网站页面
        """
        for rule in rules:
            matches = re.search(rule['SiteUrlReg'], page_url, re.DOTALL)
            if not matches:
                continue

            context = {
                "page_url":page_url,
                "rule":rule,
                "sect_data":{}
            }
            self.__open_and_read_data(context)
            return context["sect_data"]

        return None

    def __open_and_read_data(self, context):
        """
        开始读取页面数据
        """
        rule = context["rule"]
        # 执行准备操作
        self.__prepare(context)
        #开始解析页面
        for section in rule['Sections']:
            # 读取区域数据
            self.__read_section_data(section, context)

    def __prepare(self, context):
        """
        爬取前，执行准备操作
        """

        # 加载页面
        page_url = context['page_url']
        self.get(page_url)

        # 加载规则
        rule = context['rule']
        if rule.has_key('PrepareOps'):
            sections = rule['PrepareOps']
            for section in sections:
                self.__read_section_data(section, context)

        return context

    def init_context(self, page_url, rule):
        """
        初始化上下文
        """
        context = {
            "page_url":page_url,
            "rule":rule,
            "sect_data":{}
        }
        # 加载页面
        self.get(page_url)
        return context

    def perform_section(self, sections, context, parent_element=None):
        """
        执行操作区块操作
        @rule 规则
        """
        # 加载规则
        for operation in sections:
            ret = self.perform_operation(operation, context, parent_element)
            if ret == False:
                return False

        return True

    def perform_operation(self, section, context, parent_element=None):
        """
        执行操作
        @operation_name 操作名
        @context 上下文
        """

        try:
            ##
            # 操作实现
            if section['Operation'] == "SendKeys":
                element = self.__find_element(section['Selector'], parent_element)
                if element:
                    element.send_keys(section['Keys'])
            elif section['Operation'] == "Click":
                element = self.__find_element(section['Selector'], parent_element)
                if element:
                    element.click()
                elif section.has_key("CheckExist") and section["CheckExist"] == True:
                    # 如果没找到该元素而且CheckExist=True,则返回False
                    # 例如：当找不到下一页的按钮时，返回False
                    return False
            elif section['Operation'] == "ScrollUntilDone":
                self.scroll_until_done()
            elif section['Operation'] == "ScrollUntilVisible":
                self.scroll_until_visible(section['Selector'], section['TimeOut'])
            elif section['Operation'] == "Alert":
                self.show_alert(section['Message'])
            elif section['Operation'] == "Sleep":
                time.sleep(section['Time'])
            elif section['Operation'] == "OpenUrl":
                element = self.__find_element(section['Selector'], parent_element)
                if element:
                    url = self.get_element_attribute(element, section['Attribute'])
                    self.get(url)
                elif section.has_key("CheckExist") and section["CheckExist"] == True:
                    # 如果没找到该元素而且CheckExist=True,则返回False
                    # 例如：当找不到下一页的按钮时，返回False
                    return False
            ##
            # 读取DOM实现
            elif section['Operation'] == "GetFirst":
                if section['Save']:
                    # 如果该数据在上下文中已经存在，不读取任何数据
                    if context["sect_data"].has_key(section['Name']):
                        return
                    element = self.__find_element(section['Selector'], parent_element)
                    if element:
                        data = self.get_element_attribute(element, section['Attribute'])
                        context["sect_data"][section['Name']] = data
                    else:
                        context["sect_data"][section['Name']] = ""

            elif section['Operation'] == "CaptureImage":
                if not context["sect_data"].has_key(section['Name']):
                    context["sect_data"][section['Name']] = ""
                elements = self.__find_elements(section['Selector'], parent_element)
                if elements:
                    for element in elements:
                        if section.has_key('TopMargin'):
                            top_margin = section['TopMargin']
                        else:
                            top_margin = 0
                        self.scroll_to_element(element, top_margin)
                        filename = self.capture_element_view(element, section['ImgDir'])
                        if section.has_key("Attribute"):
                            attr = self.get_element_attribute(element, section['Attribute'])
                            context["sect_data"][section['Name']] += attr + "|" + filename + ";"
                        else:
                            context["sect_data"][section['Name']] += filename + ";"

            elif section['Operation'] == "SaveImage":
                if not context["sect_data"].has_key(section['Name']):
                    context["sect_data"][section['Name']] = ""

                elements = self.__find_elements(section['Selector'], parent_element)
                if elements:
                    for element in elements:
                        image_url = self.get_element_attribute(element, section['Attribute'])
                        filename = self.save_image(image_url, section['ImgDir'])
                        context["sect_data"][section['Name']] += image_url + "|" + filename + ";"
        except:
            self._log.error(traceback.format_exc())
        
        return True

    def __find_element(self, selector, parent_element):
        """
        读取元素，如果有父元素，则读取父元素下的元素
        """
        if  parent_element:
            return self.find_sub_element(parent_element, selector)
        else:
            return self.find_element(selector)

    def __find_elements(self, selector, parent_element):
        """
        读取多个元素，如果有父元素，则读取父元素下的元素
        """
        if  parent_element:
            return self.find_sub_elements(parent_element, selector)
        else:
            return self.find_elements(selector)

    def __read_section_data(self, section, context, parent_element=None):
        """
        @brief 读取区域数据执行相应操作
        @section 区域定义
        @param context 用于返回结果
        """
        try:
            # 读取 sect_data
            sdcontext = context["sect_data"]


            # 执行操作
            if section['Operation'] == "GetSubSection":
                element = self.__find_element(section['Selector'], parent_element)
                if element:
                    sub_sections = section['Sections']
                    for sub_section in sub_sections:
                        #todo: 读取子区域
                        self.__read_section_data(sub_section, context, element)

            if section['Operation'] == "SendKeys":
                element = self.__find_element(section['Selector'], parent_element)
                if element:
                    element.send_keys(section['Keys'])

            elif section['Operation'] == "Click":
                element = self.__find_element(section['Selector'], parent_element)
                if element:
                    element.click()

            elif section['Operation'] == "ScrollUntilDone":
                self.scroll_until_done()

            elif section['Operation'] == "ScrollUntilVisible":
                self.scroll_until_visible(section['Selector'], section["TimeOut"])

            elif section['Operation'] == "Alert":
                self.show_alert(section['Message'])

            elif section['Operation'] == "Sleep":
                time.sleep(section['Time'])

            elif section['Operation'] == "CombinAll":
                if not section['Save']:
                    return

                if context.has_key(section['Name']):
                    sect_data = sdcontext[section['Name']]

                elements = self.__find_elements(section['Selector'], parent_element)
                for element in elements:
                    sect_data = sect_data  + \
                        self.get_element_attribute(element, section['Attribute'])

                sdcontext[section['Name']] = sect_data

            elif section['Operation'] == "GetFirst":
                if not section['Save']:
                    return

                # 如果该数据在上下文中已经存在，不读取任何数据
                if sdcontext.has_key(section['Name']):
                    return

                element = self.__find_element(section['Selector'], parent_element)
                if element:
                    sect_data = self.get_element_attribute(element, section['Attribute'])
                    sdcontext[section['Name']] = sect_data

            elif section['Operation'] == "SaveImage":
                if section['Save']:
                    if not sdcontext.has_key(section['Name']):
                        sdcontext[section['Name']] = ""

                elements = self.__find_elements(section['Selector'], parent_element)
                if elements:
                    for element in elements:
                        self.scroll_to_element(element)
                        image_url = self.get_element_attribute(element, section['Attribute'])
                        dirname = "images/" + util.str_md5(context['page_url'])
                        filename = image_url.replace("/", "_").replace(":", "_").replace("%", "_")
                        sect_data = os.path.join(dirname, filename)
                        self.download_image(image_url, sect_data)

                    if section['Save']:
                        sdcontext[section['Name']] = sdcontext[section['Name']]  + ";" + sect_data

            elif section['Operation'] == "OpenNextPage":
                # 由于该操作会调整到下个页面，必须保证该之前说有的页面信息已经读取完毕，建议该操作是最后一个执行
                element = self.__find_element(section['Selector'], parent_element)
                if element:
                    link = self.get_attribute(element, section['Attribute'])
                    if link:
                        if section['Save']:
                            sdcontext[section['Name']] = sect_data
                            # 设定下一页页面
                            context['page_url'] = link
                            self.__open_and_read_data(context)

            elif section['Operation'] == "iframe":
                elements = self.find_elements(section['Selector'])
                if elements:
                    for element in elements:
                        self.scroll_to_element(element)
                        self._driver.switch_to.frame(element)
                        iframe_sections = section['Sections']
                        for iframe_section in iframe_sections:
                            self.__read_section_data(iframe_section, context)
                        self._driver.switch_to.default_content()

        except:
            self._log.error(traceback.format_exc())
