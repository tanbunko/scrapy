# coding: utf-8
#import sys
"""
爬虫基类
需求：
    selenium
    PIL
    requests
"""

import hashlib
import json
#import urllib
import logging
import os
import pdb
import re
#import shutil
import time
import traceback
import platform
import tempfile
import shutil
import selenium

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.common.action_chains import ActionChains
# from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC  # available since 2.26.0
from selenium.webdriver.support.ui import \
    WebDriverWait  # available since 2.4.0
from selenium.webdriver.support.ui import Select

import requests
from PIL import Image

import util


class Spider:
    """
    Spider class
    """
    _log = logging.getLogger(__name__)

    _driver = None

    def __init__(self, browser="FireFox", device_name=None, profile_dir=None):
        """
        browser: FireFox or Chrome
        device_name:
                Google Nexus 5
                Apple iPhone 6
        """
        executable_path = None
        profile = None

        if browser == "FireFox":
            if platform.system() == "Windows":
                if selenium.__version__ >= "3":
                    executable_path = os.path.join(
                        os.path.dirname(os.path.dirname(os.path.realpath(__file__))),
                        "firefox_driver/win64/geckodriver.exe")
            elif platform.system() == "Darwin":
                executable_path = os.path.join(
                    os.path.dirname(os.path.dirname(os.path.realpath(__file__))),
                    "firefox_driver/mac/geckodriver")

            # 设定firefox配置文件
            if profile_dir:
                profile = webdriver.FirefoxProfile(profile_dir)

            self._driver = webdriver.Firefox(\
                firefox_profile=profile,\
                executable_path=executable_path)

        elif browser == "Chrome":
            if platform.system() == "Windows":
                executable_path = os.path.join(
                    os.path.dirname(os.path.dirname(os.path.realpath(__file__))),
                    "chrome_driver/win32/chromedriver.exe")
            elif platform.system() == "Darwin":
                executable_path = os.path.join(
                    os.path.dirname(os.path.dirname(os.path.realpath(__file__))),
                    "chrome_driver/mac/chromedriver")
            os.environ["webdriver.chrome.driver"] = executable_path
            chrome_options = webdriver.ChromeOptions()
            # 设定Chrome配置文件路径
            if profile_dir:
                chrome_options.add_argument("user-data-dir=" + profile_dir)

            if device_name is not None:
                mobile_emulation = {"deviceName" : device_name}
                chrome_options.add_experimental_option("mobileEmulation", mobile_emulation)

            self._driver = webdriver.Chrome(
                executable_path=executable_path,
                chrome_options=chrome_options,
                desired_capabilities=chrome_options.to_capabilities(),
                service_log_path="chrome_server.log")

        # 为了避免某些链接访问时间过长，需要设置socket的Timeout时间
        # socket.setdefaulttimeout(10)
        self._driver.set_page_load_timeout(120)
        self._wait = WebDriverWait(self._driver, 20)

    def __del__(self):
        if self._driver is not None:
            self._driver.quit()
            self._driver = None


    def get(self, url):
        """
        Load page url and wait until timeout(10)
        """
        try:
            self._driver.get(url)
        except TimeoutException:
            #当页面加载时间超过设定时间，通过执行Javascript来stop加载，即可执行后续动作
            self._driver.execute_script('window.stop()')


    # title_is
    # title_contains
    # presence_of_element_located
    # visibility_of_element_located
    # visibility_of
    # presence_of_all_elements_located
    # text_to_be_present_in_element
    # text_to_be_present_in_element_value
    # frame_to_be_available_and_switch_to_it
    # invisibility_of_element_located
    # element_to_be_clickable - it is Displayed and Enabled.
    # staleness_of
    # element_to_be_selected
    # element_located_to_be_selected
    # element_selection_state_to_be
    # element_located_selection_state_to_be
    # alert_is_present

    def presence_of_element_located(self, css_selector):
        """Wait until element be presence"""
        try:
            element = self._wait.until(EC.presence_of_element_located(
                (By.CSS_SELECTOR, css_selector)))
            return element
        except NoSuchElementException:
            return False
        except TimeoutException:
            return False

    def is_visable(self, css_selector):
        """Wait until element be visable or timeout."""
        try:
            self._wait.until(EC.visibility_of_element_located(
                (By.CSS_SELECTOR, css_selector)))
            return True
        except NoSuchElementException:
            return False
        except TimeoutException:
            return False

    def scroll_to_element(self, element, top_margin=0):
        """
        Scroll to element
        """
        self._driver.execute_script("arguments[0].scrollIntoView();window.scrollBy(0, arguments[1]);", element, -top_margin)

    def scroll_until_visible(self, css_selector, timeout):
        """ scroll untill element be visable """
        self._driver.set_window_size(1200, 900)
        jssrc = """
            (function () {
                var y = 0;
                var step = 50;
                var timer = 0;
                window.scroll(0, 0);
                function f() {
                    e = document.querySelector("#css_selector#")
                    if (e && e.offsetParent) {
                        window.scroll(0, 0);
                        document.title += "scroll-done";
                    } else {
                        y += step;
                        window.scroll(0, y);
                        timer += 100;
                        if(timer > #timeout#) {
                            window.scroll(0, 0);
                            document.title += "scroll-done"
                        }
                        setTimeout(f, 100);
                    }
                }
            
                setTimeout(f, 1000);
            })();
        """

        self._driver.execute_script(jssrc.replace("#css_selector#", css_selector).\
            replace("#timeout#", str(timeout)))

        for i in xrange(10000):
            if "scroll-done" in self._driver.title:
                break
            time.sleep(1)


    def scroll_until_done(self):
        """ scroll untill to bottom """
        self._driver.set_window_size(1200, 900)
        self._driver.execute_script("""
            (function () {
                var y = 0;
                var step = 50;
                window.scroll(0, 0);
            
                function f() {
                    if (y < document.body.scrollHeight) {
                        y += step;
                        window.scroll(0, y);
                        setTimeout(f, 100);
                    } else {
                        window.scroll(0, 0);
                        document.title += "scroll-done";
                    }
                }
            
                setTimeout(f, 1000);
            })();
        """)

        for i in xrange(10000):
            if "scroll-done" in self._driver.title:
                break
            time.sleep(1)

    def is_clickable(self, css_selector):
        """ Wait until element be clickable or timeout."""
        try:
            element = self._wait.until(
                EC.presence_of_element_located((By.CSS_SELECTOR, css_selector)))
            self._wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, element)))
            return True
        except NoSuchElementException:
            return False
        except TimeoutException:
            return False

    def click_element_until_clickable(self, css_selector):
        """
        click_element_until_clickable
        """
        try:
            element = self._wait.until(
                EC.presence_of_element_located((By.CSS_SELECTOR, css_selector)))
            self._wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, element)))
            element.click()
            return True
        except NoSuchElementException:
            return False
        except TimeoutException:
            return False

    def get_all_elements_until_presence(self, css_selector):
        """ Wait until get all elements presence or timeout."""
        try:
            elements = self._wait.until(
                EC.presence_of_all_elements_located((By.CSS_SELECTOR, css_selector)))
            return elements
        except NoSuchElementException:
            return None
        except TimeoutException:
            return False

    def show_alert(self, msg):
        """
        显示信息
        """
        self._driver.execute_script("alert('" + msg + "');")


    def capture(self, url, save_fn="capture.png"):
        """ Capture the speacified url page to png """
        self._driver.set_window_size(1200, 900)
        self._driver.get(url) # Load page
        self._driver.execute_script("""
            (function () {
            var y = 0;
            var step = 100;
            window.scroll(0, 0);
        
            function f() {
                if (y < document.body.scrollHeight) {
                y += step;
                window.scroll(0, y);
                setTimeout(f, 50);
                } else {
                window.scroll(0, 0);
                document.title += "scroll-done";
                }
            }
        
            setTimeout(f, 1000);
            })();
        """)

        for i in xrange(30):
            if "scroll-done" in self._driver.title:
                break
            time.sleep(1)

        self._driver.save_screenshot(save_fn)

    def find_element_by_xpath(self, xpath):
        try:
            element = self._driver.find_element_by_xpath(xpath)
            return element
        except NoSuchElementException:
            return None
        except TimeoutException:
            return None

    def find_elements_by_xpath(self, xpath):
        try:
            elements = self._driver.find_elements_by_xpath(xpath)
            return elements
        except NoSuchElementException:
            return None
        except TimeoutException:
            return None
    
    def find_element(self, css_selector):
        try:
            element = self._driver.find_element_by_css_selector(css_selector)
            return element
        except NoSuchElementException:
            return None
        except TimeoutException:
            return None

    def find_elements(self, css_selector):
        try:
            elements = self._driver.find_elements_by_css_selector(css_selector)
            return elements
        except NoSuchElementException:
            return None
        except TimeoutException:
            return None

    def find_sub_element(self, element, css_selector):
        try:
            e = element.find_element_by_css_selector(css_selector)
            return e
        except NoSuchElementException:
            return None
        except TimeoutException:
            return None

    def find_sub_elements(self, element, css_selector):
        try:
            es = element.find_elements_by_css_selector(css_selector)
            return es
        except NoSuchElementException:
            return None
        except TimeoutException:
            return None

    def get_element_innerText(self, element):
        try:
            return element.get_attribute("innerText")
        except Exception, e:
            self._log.error(e)
        return ""

    def get_element_innerHTML(self, element):
        try:
            return element.get_attribute("innerHTML")
        except Exception, e:
            self._log.error(e)
        return ""


    def get_sub_element_attribute(self, element, css_selector, attr):
        try:
            sub_element = element.find_element_by_css_selector(css_selector)
            return self.get_element_attribute(sub_element, attr)
        except NoSuchElementException:
            return ""


    def get_element_attribute(self, element, attr):
        av = ""
        try:
            av = element.get_attribute(attr)
        except Exception, e:
            self._log.error(e)
        return av

    def get_innerText(self, css_selector):
        element = self.find_element(css_selector)
        return self.get_element_attribute(element, "innerText")

    def get_innerHTML(self, css_selector):
        element = self.find_element(css_selector)
        return self.get_element_attribute(element, "innerHTML")

    def get_attribute(self, css_selector, attr):
        element = self.find_element(css_selector)
        return self.get_element_attribute(element, attr)

    def hover_element(self, element):
        """
        鼠标悬停
        """
        hov = ActionChains(self._driver).move_to_element(element)
        hov.perform()

    def hover(self, css_selector):
        """
        鼠标悬停
        """
        element = self.find_element(css_selector)
        self.hover_element(element)

    def capture_element(self, css_selector, image_file):
        """ Capture screen shot of the element by specified css_selector
        """
        element = self.find_element(css_selector)
        if element:
            dirname = os.path.dirname(image_file)
            if not os.path.exists(dirname):
                os.makedirs(dirname)

            self._driver.save_screenshot(image_file)

            location = element.location
            size = element.size

            imfull = Image.open(image_file) # uses PIL library to open image in memory

            left = int(location['x'])
            top = int(location['y'])
            right = left + int(size['width'])
            bottom = top + int(size['height'])

            imclip = imfull.crop((left, top, right, bottom)) # defines crop points
            imclip.save(image_file, format='JPEG') # saves new cropped image


    def capture_rect(self, left, top, width, height, image_file):
        """ Capture screen shot of the element by specified css_selector
        """
        dirname = os.path.dirname(image_file)
        if not os.path.exists(dirname):
            os.makedirs(dirname)

        self._driver.save_screenshot(image_file)

        imfull = Image.open(image_file) # uses PIL library to open image in memory

        left = int(left)
        top = int(top)
        right = left + int(width)
        bottom = top + int(height)

        imclip = imfull.crop((left, top, right, bottom)) # defines crop points
        imclip.save(image_file, format='JPEG') # saves new cropped image

    def get_scroll_y(self):
        scroll = self._driver.execute_script("return window.scrollY;")
        return scroll

    def capture_element_view(self, element, out_dir):
        """ 
        截取指定元素的区域图片
        并返回图片文件路径
        """
        if element:
            if not os.path.exists(out_dir):
                os.makedirs(out_dir)
            location = element.location
            size = element.size

            if int(size['width']) <= 0 or int(size['height']) <= 0:
                return ""
            
            scroll = self.get_scroll_y()

            temp_file_name = tempfile.mktemp()
            self._driver.save_screenshot(temp_file_name)

            imfull = Image.open(temp_file_name) # uses PIL library to open image in memory

            left = int(location['x'])
            top = int(location['y'])  - scroll
            right = left + int(size['width'])
            bottom = top + int(size['height'])

            if right > imfull.size[0]:
                right = imfull.size[0]
            if bottom > imfull.size[1]:
                bottom = imfull.size[1]

            imclip = imfull.crop((left, top, right, bottom)) # defines crop points            
            output_filename = out_dir + "/" + util.file_md5(temp_file_name) + ".png"
            imclip.save(output_filename, format='png') # saves new cropped image
            os.remove(temp_file_name)
            return output_filename
        else:
            return ""

    def download_image(self, img_url, output_file):
        """ Downloads a single image.
            Downloads img_url using self.page_url as base.
            Also, raises the appropriate exception if required.
        """
        max_filesize = 3 * 1024 * 1024
        if not img_url:
            return False

        out_dir = os.path.dirname(output_file)
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)

        img_request = None
        try:
            img_request = requests.request('get', img_url, stream=True)
            if img_request.status_code != 200:
                return False
        except Exception, e:
            self._log.error(e)
            return False

        if img_url[-3:] == "svg" or int(img_request.headers['content-length']) < max_filesize:
            img_content = img_request.content
            with open(output_file, 'wb') as f:
                byte_image = bytes(img_content)
                f.write(byte_image)
                f.close()
        else:
            return False
        return True

    def save_image(self, img_url, out_dir):
        """ 保存网络图片到指定文件夹，并返回文件名.
            @img_url 图片链接
            @out_dir 保存文件夹
            @return 保存的图片路径
        """
        max_filesize = 3 * 1024 * 1024
        if not img_url:
            return ""

        if not os.path.exists(out_dir):
            os.makedirs(out_dir)

        try:
            img_request = requests.request('get', img_url, stream=True, verify=False)
            if img_request.status_code != 200:
                return ""

            regex = r".*[.]+([a-zA-Z]*)"

            matches = re.search(regex, img_url, re.DOTALL)

            if matches:
                ext = matches.group(1)
            else:
                ext = "jpg"

            if ext == "svg" or int(img_request.headers['content-length']) < max_filesize:
                img_content = img_request.content
                md5 = hashlib.md5()
                md5.update(img_content)
                output_file = out_dir + "/" + md5.hexdigest() + "." + ext
                with open(output_file, 'wb') as f:
                    byte_image = bytes(img_content)
                    f.write(byte_image)
                    f.close()
                return output_file
            else:
                self._log.error("无法下载大于3M的图片")
        except Exception, e:
            self._log.error(e)

        return ""

    def set_element_attr(self, element, attr_name, attr_value):
        """
        set element attribute
        """
        self._driver.execute_script("arguments[0].setAttribute(arguments[1], arguments[2]);", \
            element, attr_name, attr_value)

    def get_select(self, css_selector):
        """
        get select by css
        """
        select_element = self.find_element(css_selector)
        if select_element is not None:
            return Select(select_element)
        else:
            return None


