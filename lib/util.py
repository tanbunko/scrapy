"""
Utility for spider
"""
import os
import hashlib

def file_md5(filename):
    """
    get md5 of the file content
    """
    if not os.path.isfile(filename):
        return ""
    myhash = hashlib.md5()

    with open(filename, 'rb') as input_file:
        buf = input_file.read(8096)
        while buf:
            myhash.update(buf)
            buf = input_file.read(8096)
        input_file.close()
    return myhash.hexdigest()

def str_md5(content):
    """
    return the md5 of @content
    """
    md5 = hashlib.md5()
    md5.update(content)
    return md5.hexdigest()
