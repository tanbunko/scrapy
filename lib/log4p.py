#coding: utf-8
"""
日志类
"""
import logging
import logging.config

class LFFormatter(logging.Formatter):
    """
    LFFormatter
    """

    def __init__(self, default):
        self.default = default

    def format(self, record):
        if isinstance(record.msg, str) or isinstance(record.msg, unicode):
            # 将日志信息中的回车替换为{{LF}}
            record.msg = record.msg.replace("\n", "{{LF}}")
        return self.default.format(record)

def lffmt(fmt, datefmt):
    """
    lffmt
    """
    default = logging.Formatter(fmt, datefmt)
    return LFFormatter(default)

class SingleLevelFilter(object):
    """
    SingleLevelFilter
    """

    def __init__(self, pass_level):
        self.pass_level = pass_level

    def filter(self, record):
        """
        filter
        """
        if self.pass_level == record.levelno:
            return True
        return False

class ScreenHandler(logging.StreamHandler):
    """
    ScreenHandler
    """
    def emit(self, record):
        try:
            msg = self.format(record)
            stream = self.stream
            fs = u"%s\n"
            try:
                if isinstance(msg, unicode) and getattr(stream, 'encoding', None):
                    ufs = fs.decode(stream.encoding)
                    try:
                        stream.write(ufs % msg)
                    except UnicodeEncodeError:
                        stream.write((ufs % msg).encode(stream.encoding))
                else:
                    stream.write(fs % msg)
            except UnicodeError:
                #stream.write(fs % msg.encode("UTF-8"))
                print msg
            self.flush()
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            self.handleError(record)

def init_logger_by_dict():
    conf = {
        'version': 1,
        'disable_existing_loggers': True,
        'incremental': False,
        'formatters':
        {
            'simple': {
                'class': 'logging.Formatter',
                'format': '%(filename)s:%(lineno)s %(levelname)s %(message)s',
                'datefmt': '%Y-%m-%d %H:%M:%S'
            },
            "full":{
                "format":'%(asctime)s,%(process)d,%(filename)s,%(lineno)s,%(name)s,%(levelname)s,"""%(message)s"""',
                "class": lffmt,
            }
        },
        'filters':
        {
            'filter_by_name':
            {
                'class': 'logging.Filter',
                'name': 'logger_for_filter_name'
            },

            'filter_single_level_pass':
            {
                'class': 'log4p.SingleLevelFilter',
                'pass_level': logging.WARN
            }
        },
        'handlers':
        {
            'console':
            {
                'class': 'logging.StreamHandler',
                'level': 'INFO',
                'formatter': 'simple',
                'filters': ['filter_single_level_pass', ]
            },
            'screen':
            {
                'class': 'log4p.ScreenHandler',
                'level': logging.INFO,
                'formatter': 'simple',
                'filters': ['filter_by_name', ]
            },
            "info_file_handler":
            {
                "class": "logging.handlers.RotatingFileHandler",
                "level": "INFO",
                "formatter": "full",
                "filename": "info.log",
                "maxBytes": 10485760,
                "backupCount": 20,
                "encoding": "utf8"
            },
        },
        'loggers':
        {
            'logger_for_filter_name':
            {
                'handlers': ['console', 'screen'],
                'filters': ['filter_by_name', ],
                'level': 'INFO'
            },
            'logger_for_all':
            {
                'handlers': ['console', ],
                'filters': ['filter_single_level_pass',],
                'level': 'INFO',
                'propagate': False
            },
            "": {
                'handlers': ['console', 'info_file_handler'],
                'level': 'INFO',
                'propagate': False
            },
        }
    }

    logging.config.dictConfig(conf)
