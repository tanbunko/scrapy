
import os
import requests
import hashlib
import pdb
import tempfile
from PIL import Image


def download_image(img_url, out_dir):
    """ Downloads a single image.
        Downloads img_url using self.page_url as base.
        Also, raises the appropriate exception if required.
    """
    max_filesize = 3 * 1024 * 1024
    if not img_url:
        return False

    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    img_request = None
    try:
        proxies={'sokets' : '127.0.0.1:1080'}
        img_request = requests.request('get', img_url, stream=True, verify=False, proxies=proxies)
        if img_request.status_code != 200:
            return False
    except Exception, e:
        # self._log.error(e)
        return False

    if img_url[-3:] == "svg" or int(img_request.headers['content-length']) < max_filesize:
        img_content = img_request.content
        md5 = hashlib.md5()
        md5.update(img_content)
        output_file = out_dir + "/" + md5.hexdigest() + "." + img_url[-3:]

        with open(output_file, 'wb') as img_fp:
            byte_image = bytes(img_content)
            img_fp.write(byte_image)
            img_fp.close()
        return output_file
    else:
        return False
    return True


def get_file_md5(filename):
    if not os.path.isfile(filename):
        return ""
    myhash = hashlib.md5()

    with open(filename, 'rb') as f:
        b = f.read(8096)
        while b:
            myhash.update(b)
            b = f.read(8096)
        f.close()
    return myhash.hexdigest()


# filename = tempfile.mkdtemp()
# print filename

# file1 = download_image("https://pbs.twimg.com/media/CRf68VAVEAE_UxP.jpg", "output")
# print file1

a = "abc"
a += "efg"
print a
