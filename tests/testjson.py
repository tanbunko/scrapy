import json
import re


if __name__ == '__main__':
    infile = open("tests/site_rule.json", 'rb')
    jsonObj = json.load(infile)
    infile.close()

    firstObj = jsonObj[0]


    test_str = "http://www.afpbb.com/articles/-/3118395"

    matches = re.search(firstObj['SiteUrlReg'], test_str, re.DOTALL)

    if matches:
        print ("Match was found at {start}-{end}: {match}".\
            format(start = matches.start(), end = matches.end(), match = matches.group()))

        for groupNum in range(0, len(matches.groups())):
            groupNum = groupNum + 1        
            print ("Group {groupNum} found at {start}-{end}: {group}".\
                format(groupNum = groupNum, start = matches.start(groupNum), \
                    end = matches.end(groupNum), group = matches.group(groupNum)))