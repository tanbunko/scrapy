#encoding:utf8
import sys
sys.path.append("..")


from lib.spider import Spider

class TestSpider(Spider):

    def test_message(self, msg):
        self._driver.get("http://gou.jd.com/exp4?cu=true&utm_source=offlintab.firefoxchina.cn&utm_medium=uniongou&utm_campaign=t_220520384_&utm_term=0798a5f9fa3a45b0a0bc05347b5adb56&abt=3")
        items = self.get_all_elements_until_presence("div#recommList div.vb_item")
        for item in items:
            e_img = self.find_sub_element(item, "a img.vb_img")
            img_url = self.get_attribute(e_img, "src")
            self.save_link_image(img_url)

    def test_mobile(self):
        self._driver.get("http://www.yahoo.co.jp")



if __name__ == '__main__':
    #a = TestSpider()

    #a.test_message("test")

    a = TestSpider("Google Nexus 5")

    a.test_mobile()
