#encoding:utf8
import sys
import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)

import lib

import logging

if __name__ == '__main__':
    #输出根日志
    log = logging.getLogger(__name__)
    log.debug("debug message")
    log.info(u"Information \n わかりました")
    log.error("error")

    #输出爬虫日志
    scrapy_log = logging.getLogger("Spider")
    scrapy_log.debug(u"scrapy debug \n 消息 ")
    scrapy_log.info("scrapy info message")
    scrapy_log.error("scrapy error message")
