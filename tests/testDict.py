d1 = {"a": "this is a", "b":"this is b"}
d2 = {"b": "this is b in d2", "d":"this is d"}

d3 = dict(d1, **d2)
c = d3['b']
print c
c = "ok"

print c
print d3['b']

d3['b'] = "cd"

print c
print d3['b']

print d3
