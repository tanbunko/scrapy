import urllib

import requests
import os
                       
src = "https://images-na.ssl-images-amazon.com/images/I/41aQXOfPBfL._SY355_.jpg"

def download_image(img_url):
    """ Downloads a single image.
        Downloads img_url using self.page_url as base.
        Also, raises the appropriate exception if required.
    """
    max_filesize = 9000000
    img_request = None
    try:
        img_request = requests.request('get', img_url, stream=True)
        if img_request.status_code != 200:
            return False
    except Exception, e:
        print e.msg
        return False

    if img_url[-3:] == "svg" or int(img_request.headers['content-length']) < max_filesize:
        img_content = img_request.content
        with open(os.path.join("", img_url.split('/')[-1]), 'wb') as f:
            byte_image = bytes(img_content)
            f.write(byte_image)
    else:
        return False
    return True

# download the image
download_image("https://images-na.ssl-images-amazon.com/images/I/41aQXOfPBfL._SY355_.jpg")

