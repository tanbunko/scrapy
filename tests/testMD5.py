import md5
import hashlib


src = 'this is a md5 test.'   
m1 = md5.new()   
m1.update(src)   
print m1.hexdigest()


m2 = hashlib.md5()   
m2.update(src)   
print m2.hexdigest()
