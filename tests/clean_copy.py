#code:utf8
'''
Delete dest directory & copy src direcotry
'''
import os
import shutil

SRC_DIR = "C:\\prjs\\jcshop\\upload\\output.201611161058"
DEST_DIR = "C:\\prjs\\jcshop\\upload\\goods_pic\\mainPic"

for filename in os.listdir(SRC_DIR):
    src_target = os.path.join(SRC_DIR, filename)
    dest_target = os.path.join(DEST_DIR, filename)

    if os.path.isdir(src_target):
        if os.path.isdir(dest_target):
            shutil.rmtree(dest_target)
        print '*'
        shutil.copytree(src_target, dest_target)


